:gitlab_url: https://gitlab.com/molpro/pysjef_molpro

=======================================
Welcome to pysjef_molpro documentation!
=======================================

pysjef_molpro is an extension of `pysjef <https://molpro.gitlab.io/pysjef/>`_
for working with `Molpro quantum chemistry package <https://www.molpro.net/>`_.

Installation
------------


Table of Contents
=================
.. toctree::
    :maxdepth: 2

    project
    output

